<?php
//boolean example started from here
$bool=true;
if($bool) echo "bool is true";
$bool=false;
if($bool) echo "bool is false";
//boolean example ended from here

//integer  example started from here
$value =10;
echo $value ."<br>";//.<br>-line a break  print
//integer example started from here

//float example started from here
$value =10.99;
echo $value ."<br>";
//float example started from here

//string example started from here
$val =10;
 $value1=" double quoted string $val <br>";
$value2='double quoted string $val <br>';
echo $value1.$value2;

$HEREDOCSTRING=<<<BITM
        HEREDOCS EXAMPLE LINE 1 $val <br>
         HEREDOCS EXAMPLE LINE 2 $val <br>
          HEREDOCS EXAMPLE LINE 3 $val <br>

 BITM;

 $NOWDOCSTRING=<<<'BITM'
          NOWDOCS EXAMPLE LINE 1 $val <br>
         NOWEDOCS EXAMPLE LINE 2 $val <br>
          NOWEDOCS EXAMPLE LINE 3 $val <br>



BITM;

echo $HEREDOCSTRING."<br> <br>".$NOWDOCSTRING;

//string example started from here

$arr= array(1,2,3,4);
print_r($arr);
echo "<br>";
$arr= array("bmw","toyota","nano");
print_r($arr);
$arr= array("bmw"=>30,"toyota"=>22,"nano"=>33);
//print_r($arr);
echo "the age is:".$arr["bmw"];